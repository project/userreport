<?php
/**
 * @file
 */

/**
 * Implements hook_admin_form().
 */
function userreport_admin_form($form, &$form_state) {
  if (module_exists('googleanalytics') AND variable_get('googleanalytics_account', '') == '') {
    drupal_set_message(t('You have enabled the Google Analytics module, but have not setup your account yet. <a href="@url">Configure Google Analytics</a>.', array('@url' => $base_path . 'admin/config/system/googleanalytics')), 'warning');
  }
  $form['userreport'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Configuration for Userreport.com integration'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['userreport']['userreport_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#required' => TRUE,
    '#default_value' => variable_get('userreport_key', ''),
  );
  if (!module_exists('googleanalytics')) {
    $form['userreport']['userreport_gacode'] = array(
      '#type' => 'textfield',
      '#title' => t('Google Analytics Code'),
      '#required' => FALSE,
      '#default_value' => variable_get('userreport_gacode'),
    ); 
  }
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#executes_submit_callback' => TRUE,
  );
  $form['#submit'] = array(
    'userreport_admin_form_submit'
  );
  return $form;
}

/**
 * Implements hook_admin_validate().
 */
function userreport_admin_validate($form, &$form_state) {
  $key = $form_state['values']['userreport_key'];
  if (!is_string($key)) {
    form_set_error('userreport_key', t('The key is not correctly setup'));
  }
}

/**
 * Implements hook_admin_submit().
 */
function userreport_admin_form_submit($form, &$form_state) {
  variable_set('userreport_key', $form_state['values']['userreport_key']);
  if (!module_exists('googleanalytics') AND !empty($form_state['values']['userreport_gacode'])) {
    variable_set('userreport_gacode', $form_state['values']['userreport_gacode']);
  }
  drupal_set_message(t("Settings has been updated"));
}
