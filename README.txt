Userreport.com
==============

Enables <a href="http://userreport.com">Userreport.com</a> on you website by adding the required javascript for the service. 

The service makes it possible to:

- conduct user surveys
- collect user feedback
- transfer background about your visitors to Google Analytics

First you need to create your project at http://userreport.com. Then enable this module, fill in the configuration options at admin/config/system/userreport, and you are good to go.

Integrates automatically with the <a href="http://drupal.org/project/google_analytics">Google Analytics</a> module.

I accept both co-maintainers and patches to improve the module.

This module has been sponsored by <a href="http://vih.dk">Vejle Idrætshøjskole</a> and <a href="http://discimport.dk">Discimport.dk</a>.


